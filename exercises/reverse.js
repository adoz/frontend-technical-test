arr = [
	"Hello world", // -> "world Hello"
	"I hate doing technical tests", // -> "tests technical doing hate I"
	"Holaluz is a great place to work", // -> "work to place great a is Holaluz"
]

function reverseWordsInSentence(sentence) {
	const words = sentence.split(' ');
	return words.reverse().join(' ');
}

for (var i in arr) {
	reverseWordsInSentence(arr[i]);
}